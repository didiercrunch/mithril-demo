# Demo of a basic mithril application

## Building

`nix build` will produce a reproducible build of the
output site.

## Developing Environment
All development tools should be included with in the `nix develop`
shell session.  Basically, it requires parcel, node, npm and deno.

### Running Test

`deno test` should run all the tests.

### Dev server

Running `parcel` should do the job.
