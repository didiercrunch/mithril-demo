import m from 'npm:mithril';
import {CacheService, Starship, StarWarsApiService, StarWarsService} from "./services.ts";

enum Errors {
    StarshipNotFound = 0,
}

interface Services {
    starWarsService: StarWarsService;
    starshipsData: StarshipsData;
}


function createAppServices(): Services {
    const cacheService = new CacheService(m.redraw);
    const starWarsApiService = new StarWarsApiService(cacheService.cacheUrlFetcher(m.request), cacheService);
    const starWarsService = new StarWarsService(starWarsApiService);
    const starshipsData = new StarshipsData(starWarsService);
    return {starWarsService, starshipsData};
}


class StarshipsData {
    readonly starWarsService: StarWarsService;
    readonly ships: Starship[] = []
    readonly shipByUrl: {string: Starship|Errors} = {};

    constructor(starWarsService: StarWarsService) {
        this.starWarsService = starWarsService;
    }

    public async loadShips(): Promise<void>{
        if(this.ships.length > 0){
            return;
        }
        for await (const ship of this.starWarsService.getStarships()){
            this.ships.push(ship)
        }
    }

    public async loadShip(url: string): Promise<void>{
        const ship = this.shipByUrl[url]
        if(ship){
            return ship;
        }
        const ret = await this.starWarsService.getStarship(url);
        if(!ret){
            this.shipByUrl[url] = Errors.StarshipNotFound;
        }
        this.shipByUrl[url] = ret;

    }
}


class StarshipsView{
    readonly starshipsData: StarshipsData;

    constructor({attrs}: m.CVnode<Services>) {
        this.starshipsData = attrs.starshipsData;
    }

    creatShipRow = (ship: Starship, idx: number) => {
        return m('tr',
            m('td', idx),
            m('td',  m(m.route.Link,
                       {href: "/starships/:shipUrl", params: {shipUrl: ship.url}},
                       ship.shipName)));
    };

    oninit = async () => {
        await this.starshipsData.loadShips();
    };

    view(){
        return m('div',
            m('h1', 'Starships'),
            m('table.table.is-striped.is-hoverable.is-fullwidth',
                this.starshipsData.ships.map(this.creatShipRow)));
    }
}


class StarshipView{
    readonly starshipsData: StarshipsData;
    constructor(vnode: m.CVnode<Services>) {
        this.starshipsData = vnode.attrs.starshipsData;
        this.url = vnode.attrs.shipUrl;
    }

    oninit = async (vnode) => {
        await this.starshipsData.loadShip(this.url);
    };

    getStarship() :  undefined | Errors | Starship {
        return this.starshipsData.shipByUrl[this.url];
    }

    view(){
        const ship = this.getStarship();
        if(!ship){
            return m('div', 'loading data');
        }
        if(ship === Errors.StarshipNotFound){
            return m('div', '404 not found');
        }
        return m('div',
            m('table.table.is-striped.is-hoverable.is-fullwidth',
                m('caption', ship.shipName),
                m('tr', m('td', 'Length'), m('td', '' + ship.shipLength)),
                m('tr', m('td', 'Number of crew'), m('td',  !ship.crew ? 'Unknown' : `${ship.crew.min} to ${ship.crew.max}`)))

        );
    }
}

class PilotView{
    view(vnode){
        return m('div', [
            m('h1', `Pilot ${vnode.attrs.pilotId}`),
        ]);
    }
}

function injectServices(component, services){
    return {
        view: (vnode) => {
            return m(component, {...vnode.attrs, ...services});
        }
    };
}

function main(root: HTMLElement){
    const services = createAppServices()
    m.route(root, "/", {
        "/starships": injectServices(StarshipsView, services),
        "/starships/:shipUrl": injectServices(StarshipView, services),
        "/pilots/:pilotId": PilotView,
        "/": injectServices(StarshipsView, services),
    });
}

main(document.body);