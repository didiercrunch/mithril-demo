
type UrlFetcher = (url: string) => Promise<any>;
export interface StarshipApiDTO {
  name: string,
  length: string,
  crew: string,
  url: string,
}

export class Range {
  public readonly min: number;
  public readonly max: number;

  constructor(min: number, max: number) {
    this.min = min;
    this.max = max;
  }

  static fromString(s: string): Range | undefined {
    const re = /^(\d+)-(\d+)$/;
    const ret = re.exec(s);
    return !ret ? undefined : new Range(parseInt(ret[1]), parseInt(ret[2]));
  }
}


export class Starship {
  readonly shipName: string;
  readonly shipLength: number;
  readonly crew?: Range;
  readonly url: string;


  constructor(shipName: string, shipLength: number, url: string, crew?: Range) {
    this.shipName = shipName;
    this.shipLength = shipLength;
    this.crew = crew;
    this.url = url
  }

  static fromApiDTO(dto: StarshipApiDTO): Starship | undefined {
    return new Starship(
        dto.name,
        parseInt(dto.length),
        dto.url,
        Range.fromString(dto.crew),
    );
  }
}

export class CacheService {
  private readonly cache = new Map<string, any>();
  private readonly callback?: () => void;


  /**
   * @param callback will be called once a cached value is returned.
   */
  constructor(callback?: () => void) {
    this.callback = callback;
  }

  cacheUrlFetcher(fetcher: UrlFetcher): UrlFetcher {
    return async (url: string) : Promise<any> => {
      if(this.cache.has(url)){
        if(this.callback){
          this.callback();
        }
        return this.cache.get(url)
      }
      const ret = await fetcher(url);
      this.setToCache(url, ret);
      return ret;
    }
  }

  setToCache(url: string, payload: any){
    this.cache.set(url, payload);
  }

}


export interface StarWarsApiServiceInterface {
  getStarshipApiDTOs(): AsyncGenerator<StarshipApiDTO>;

  getStarshipApiDTO(url: string): Promise<StarshipApiDTO | undefined>;
}

export class StarWarsApiService implements StarWarsApiServiceInterface {
  private readonly sourceURL: string = 'https://swapi.dev/api/starships/';
  private readonly urlFetcher: (url: string) => Promise<any>;
  private readonly cacheService?: CacheService;

  constructor(urlFetcher: UrlFetcher, cacheService?: CacheService) {
    this.urlFetcher = urlFetcher;
    this.cacheService = cacheService;
  }

  private async fetchApiPage(url: string): Promise<[StarshipApiDTO[], string | undefined]> {
    let payload = await this.urlFetcher(url);
    return [payload.results as StarshipApiDTO[], payload.next]
  }

  async* getStarshipApiDTOs(): AsyncGenerator<StarshipApiDTO> {
    let ret: StarshipApiDTO[] = []
    let nextUrl: string | undefined = this.sourceURL;
    while (nextUrl) {
      [ret, nextUrl] = await this.fetchApiPage(nextUrl);
      for (const r of ret) {
        // I am 100% against setting api cache like this.
        this.cacheService?.setToCache(r.url, r);
        yield r;
      }
    }
  }

  async getStarshipApiDTO(url: string): Promise<StarshipApiDTO | undefined> {
    try {
      const ret = await this.urlFetcher(url)
      return ret as StarshipApiDTO;
    } catch (e) {
      console.error(`Damn, could not fetch data from api at ${url}.`)
      return undefined;
    }
  }
}

export class StarWarsService {
  private readonly starWarsApiService: StarWarsApiServiceInterface;


  constructor(starWarsApiService: StarWarsApiServiceInterface) {
    this.starWarsApiService = starWarsApiService;
  }

  async* getStarships(): AsyncGenerator<Starship> {
    for await (const apiDto of this.starWarsApiService.getStarshipApiDTOs()) {
      const ret = Starship.fromApiDTO(apiDto);
      if (!ret) {
        console.error("log something useful here");
        continue;
      }
      yield ret;
    }
  }

  async getStarship(url: string): Promise<Starship | undefined>{
    let ret = await this.starWarsApiService.getStarshipApiDTO(url);
    if(!ret){
      return undefined;
    }
    return Starship.fromApiDTO(ret);
  }
}