import {assertEquals} from "https://deno.land/std@0.183.0/testing/asserts.ts";
import {
  CacheService,
  Range,
  Starship,
  StarshipApiDTO,
  StarWarsApiService,
  StarWarsApiServiceInterface,
  StarWarsService
} from "../src/services.ts";
import starshipsData from './startship_data.json' assert { type: "json" };

Deno.test("Range constructor", () => {
  assertEquals(Range.fromString('foo'), undefined);
  assertEquals(Range.fromString('33-789'), new Range(33, 789));
});

Deno.test("Building Starship from api dto", () => {
  const apiDto = starshipsData[0] as StarshipApiDTO;
  const expt = new Starship("CR90 corvette", 150, 'https://swapi.dev/api/starships/2/', new Range(30, 165));
  assertEquals(Starship.fromApiDTO(apiDto), expt)

});

async function countAsyncGen(gen: AsyncGenerator): Promise<number>{
  let count = 0;
  for await (const x of gen){
    count += 1;
  }
  return count;
}


Deno.test({
  name: "Crazy test that test the api...",
  ignore: true, // this is an integration test
  fn:  async () => {
    const service = new StarWarsApiService(async (url: string) => {
      let req  = await fetch(url);
      return await req.json();
    });
    assertEquals(await countAsyncGen(service.getStarshipApiDTOs()), 36);
  }
});


Deno.test("StarWarsService", async (t: Deno.TestContext) => {
  class MockStarWarsApiServiceInterface implements StarWarsApiServiceInterface{
    async *getStarshipApiDTOs(): AsyncGenerator<StarshipApiDTO> {
      for(const apiDto of starshipsData){
        yield apiDto as StarshipApiDTO;
      }
    }

    async getStarshipApiDTO(url: string): Promise<StarshipApiDTO|undefined>{
      for(const apiDto of starshipsData){
        if(apiDto.url === url){
          return apiDto as StarshipApiDTO;
        }
      }
    }
  }

  await t.step("getStarships", async () => {
    const service = new StarWarsService(new MockStarWarsApiServiceInterface());
    const gen = service.getStarships();
    const itm = await gen.next();
    const expt = new Starship("CR90 corvette", 150, 'https://swapi.dev/api/starships/2/', new Range(30, 165));
    assertEquals(itm.value, expt);
    assertEquals(await countAsyncGen(service.getStarships()), 10);
  });

  await t.step("getStarship", async () => {
    const service = new StarWarsService(new MockStarWarsApiServiceInterface());
    let ret = await service.getStarship('https://swapi.dev/api/starships/2/');
    const expt = new Starship("CR90 corvette", 150, 'https://swapi.dev/api/starships/2/', new Range(30, 165));
    assertEquals(ret, expt);

    ret = await service.getStarship('404 not found');
    assertEquals(ret, undefined);
  });

});

Deno.test("CacheService", async (t: Deno.TestContext) =>{
  class MockFetcher{
    readonly mockFetcherCountCall: {[n: string]: number} = {};

    mockFetcher = async(url: string): Promise<any> => {
      this.mockFetcherCountCall[url] = (this.mockFetcherCountCall[url] || 0) + 1
      const countCall = this.mockFetcherCountCall[url];
      return {url, countCall};
    }

  }

  await t.step('no callback', async () => {
    const cacheService = new CacheService();
    const mf = cacheService.cacheUrlFetcher(new MockFetcher().mockFetcher);

    assertEquals(await mf('foo'), {url: "foo", countCall: 1})
    assertEquals(await mf('foo'), {url: "foo", countCall: 1})

    cacheService.setToCache('toto', 'titi')
    assertEquals(await mf('toto'), 'titi')
  });

  await t.step('with callback', async () => {
    let callbackCount = 0;
    const callback = function(){
      callbackCount += 1;
    }

    const cacheService = new CacheService(callback);
    const mf = cacheService.cacheUrlFetcher(new MockFetcher().mockFetcher);

    assertEquals(await mf('foo'), {url: "foo", countCall: 1})
    assertEquals(callbackCount, 0);
    assertEquals(await mf('foo'), {url: "foo", countCall: 1})
    assertEquals(callbackCount, 1);

    cacheService.setToCache('toto', 'titi')

    assertEquals(await mf('toto'), 'titi')
    assertEquals(callbackCount, 2);
  });
});



