{
  description = "Demo of a mithril based frontend application.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        node-modules = pkgs.mkYarnPackage {
          name = "node-modules";
          src = ./.;
        };
        frontend = pkgs.stdenv.mkDerivation {
          name = "frontend";
          src = ./.;
          buildInputs = [pkgs.yarn node-modules];
          buildPhase = ''
            ln -s ${node-modules}/libexec/mithril-demo/node_modules node_modules
            ${pkgs.yarn}/bin/yarn build
          '';
          installPhase =  ''
          mkdir $out
          mv dist/* $out
          '';

        };
      in 
        {
          packages = {
            node-modules = node-modules;
            frontend = frontend;
            default = frontend;
          };
          devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [nodejs-19_x
                                      deno
                                      nodePackages.parcel];
          };
        }
    );
}
